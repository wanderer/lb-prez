# Dracula for [Beamer](https://ctan.org/pkg/beamer?lang=en)

> A dark theme for [Beamer](https://ctan.org/pkg/beamer?lang=en).

|Default ![](screenshot.png)  | PaloAlto ![](screenshot_2.png)  |
|-----------------|-----------------|
|CambridgeUS ![](screenshot_3.png)  |Ilmenau ![](screenshot_4.png)  |
|Warsaw ![](screenshot_5.png)  |Antibes ![](screenshot_6.png) 

## Install

All instructions can be found at [draculatheme.com/beamer](https://draculatheme.com/beamer).

## Team

This theme is maintained by the following person.

[![Bisrat Haile](https://github.com/bsrthyle.png?size=100)](https://github.com/bsrthyle) |
--- |
[Bisrat Haile](https://github.com/bsrthyle) |

## License

[MIT License](./LICENSE)